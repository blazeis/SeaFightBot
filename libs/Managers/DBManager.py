
from peewee import Model, CharField, MySQLDatabase, BigIntegerField, IntegerField, SmallIntegerField
import settings

database = MySQLDatabase(database=settings.DB_NAME,
                         user=settings.DB_USER,
                         password=settings.DB_PASS,
                         host=settings.DB_HOST,
                         port=3306)
database.connect()


class BaseModel(Model):
    class Meta:
        database = database


class User(BaseModel):
    tg_id = BigIntegerField(unique=True)
    tg_name = CharField(null=True)
    tg_user = CharField(null=True)
    access_level = IntegerField()
    code_send = IntegerField(default=0)
    email = CharField(null=True)


class Messages(BaseModel):
    date = IntegerField(default=0)
    sender_id = BigIntegerField(unique=False)
    text = CharField(null=True, max_length=4096)
    reply_to = BigIntegerField(unique=False, null=True)
    file_id = CharField(null=True, max_length=4096)
    forward_from = BigIntegerField(unique=False, null=True)
    message_type = SmallIntegerField(unique=False)


database.connect(reuse_if_open=True)
database.create_tables([User, Messages])

if not database.is_closed():
    database.close()


def manage_connection(f):
    def arg_decorator(*args, **kwargs):
        database.connect(reuse_if_open=True)
        res = f(*args, **kwargs)
        if not database.is_closed():
            database.close()
        return res
    return arg_decorator
