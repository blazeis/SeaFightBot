import logging
import os
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler, PicklePersistence

import settings
from libs.static import AccessLevel
from libs.utils import get_access_level, save_callback, save_message, get_keyboard, update_access_level
from modules.BotGame import BotGame


class BotManager:
    def __init__(self):
        if not os.path.exists('sessions'):
            os.makedirs('sessions')
        self.updater = Updater(settings.TOKEN,
                               persistence=PicklePersistence(filename=f'sessions{settings.DELIMITER}persistence_bot.pb'),
                               use_context=True)
        self.dp = self.updater.dispatcher
        self.bot = self.updater.bot
        self.plugins_callback = {}
        self.plugins_userdata = {}
        self.plugins_reply = {}

        self._game = BotGame(self.bot)

        key, method = self._game.register_plugin_callback()
        self.plugins_callback[key] = method

        key, method = self._game.register_plugin_userdata()
        self.plugins_userdata[key] = method

        key, method = self._game.register_plugin_reply()
        self.plugins_reply[key] = method

    def callback_handler(self, update, context):
        query = update.callback_query
        data = query.data
        query.answer()
        user = update.effective_user
        user_id, user_name, user_login = user.id, user.first_name, user.username
        level = get_access_level(user_id=user_id)
        try:
            save_callback(query.data, user_id)
        except Exception as e:
            logging.error(f'Cant save callback: {e}\n{query}')
        if level >= AccessLevel.USER:
            params = data.split('_')
            f_prefix = params[0]
            if len(params) > 1:
                f_name = f"handle_callback_{params[0]}_{params[1]}"
            else:
                f_name = f"handle_callback_{params[0]}"
            my_handler = getattr(self, f_name, None)
            if my_handler is not None:
                logging.info(f"{f_name} from: {user_name}({user_id}), level: {level.name}, data: {data}")
                my_handler(update, context, level, user, query)
            else:
                if f_prefix in self.plugins_callback.keys():
                    logging.info(f'executing plugin {f_prefix} callback helper')
                    self.plugins_callback[f_prefix](update, context)
                else:
                    logging.error(f"Error in handle_callback: function for {data.split('_')[0]} not found!")

    def handle_reply(self, update, context):
        logging.info('reply received')
        for key_plugin in self.plugins_reply.keys():
            searchfor = f'{key_plugin}_reply'
            found = ""
            for key in context.user_data.keys():
                if key.startswith(searchfor):
                    self.plugins_reply[key_plugin](update, context)
                    return

    def handle_message(self, update, context):
        logging.info(f'handling message: {update}')
        if update.message is not None:
            try:
                save_message(update.message)
            except Exception as e:
                logging.error(f'Cant save message: {e}\n{update.message}')
            if update.message.chat.type == 'private':
                self.handle_private_message(update, context)
            else:
                group_id = update.message.chat.id
                logging.info(f"handling message in group {group_id}")
                # save_message(update.message)

    def handle_private_message(self, update, context):
        user = update.effective_user
        user_id, user_name, user_login = user.id, user.first_name, user.username
        level = get_access_level(user_id)
        if level == AccessLevel.UNKNOWN:
            if update.message.text is not None:
                if update.message.text == 'Привет':
                    level = AccessLevel.USER
                    update_access_level(user_id, user_name, user_login, AccessLevel.USER)
                    keyboard = get_keyboard(level, 'base')
                    update.message.reply_text("Приятно познакомиться. Что делать будем?", reply_markup=keyboard)
        else:
            for key in self.plugins_userdata.keys():
                if context.user_data.get(key, None) is True:
                    logging.info(f"plugin {key} received")
                    self.plugins_userdata[key](update, context)
                    return
            keyboard = get_keyboard(level, 'base')
            update.message.reply_text("Привет. Что делать будем?", reply_markup=keyboard)

    def start_command(self, update, message):
        text = 'Это просто тестовый бот'
        level = AccessLevel.USER
        user = update.effective_user
        update_access_level(user.id, user.first_name, user.username, AccessLevel.USER)
        keyboard = get_keyboard(level, 'start')
        update.message.reply_text(text, reply_markup=keyboard)

    def run(self):
        self.dp.add_handler(CallbackQueryHandler(self.callback_handler))
        self.dp.add_handler(CommandHandler("start", self.start_command))
        self.dp.add_handler(MessageHandler(Filters.reply, self.handle_reply))
        self.dp.add_handler(MessageHandler(Filters.photo |
                                           Filters.video |
                                           Filters.video_note |
                                           Filters.animation |
                                           Filters.audio |
                                           Filters.document |
                                           Filters.sticker,
                                           self.handle_message))
        self.dp.add_handler(MessageHandler(Filters.text & ~Filters.command & ~Filters.forwarded, self.handle_message))

        self.updater.start_polling()
        self.updater.idle()
