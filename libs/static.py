from enum import IntEnum
from telegram import InlineKeyboardMarkup, InlineKeyboardButton


class AccessLevel(IntEnum):
    BANNED = -1
    UNKNOWN = 0  # мы его не знаем
    USER = 1  # все зареганные
    OPERATOR = 2  # саппорт
    ADMIN = 3  # админ


class MessageType(IntEnum):
    UNKNOWN = 0
    TEXT = 1
    AUDIO = 2
    PHOTO = 3
    VIDEO = 4
    DOCUMENT = 5
    ANIMATION = 6
    STICKER = 7
    CALLBACK = 8


kb_user = {'base': InlineKeyboardMarkup([[InlineKeyboardButton("О боте", callback_data=f'info')]]),
           'start': InlineKeyboardMarkup([[InlineKeyboardButton("Разместить мои корабли ⚔️", callback_data=f'game_place')]]),
           'placed': InlineKeyboardMarkup([[InlineKeyboardButton("Размести заново 🔄", callback_data=f'game_place')],
                                           [InlineKeyboardButton("Начинаем! 🗡", callback_data=f'game_start')]]),
           'restart': InlineKeyboardMarkup([[InlineKeyboardButton("Давай заново 🧨", callback_data=f'game_place')]])}


class Direction(IntEnum):
    VERTICAL = 0
    HORIZONTAL = 1


header = "⚔️1️⃣2️⃣3️⃣4️⃣5️⃣6️⃣7️⃣8️⃣9️⃣🔟\n"
fields = {0: "1️⃣",
          1: "2️⃣",
          2: "3️⃣",
          3: "4️⃣",
          4: "5️⃣",
          5: "6️⃣",
          6: "7️⃣",
          7: "8️⃣",
          8: "9️⃣",
          9: "🔟"}

colors = {'red': "🟥",
          'orange': "🟧",
          'yellow': "🟨",
          'green': "🟩",
          'blue': "🟦",
          'purple': "🟪",
          'black': '⬛️',
          'white': '⬜️'}


class FieldState(IntEnum):
    WATER = 0
    NEAR = 1
    SHIP = 2
    DAMAGED = 3
    KILLED = 4
    MISSED = 5


symbols = {FieldState.WATER: colors['blue'],
           FieldState.NEAR: colors['blue'],
           FieldState.SHIP: colors['orange'],
           FieldState.DAMAGED: colors['purple'],
           FieldState.KILLED: colors['red'],
           FieldState.MISSED: colors['white']}
