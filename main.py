import logging
from libs.Managers.BotManager import BotManager


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

bot = BotManager()

if __name__ == '__main__':
    logger.info('starting bot..')
    bot.run()
