import logging

from libs.static import AccessLevel, MessageType
from libs.utils import get_access_level, reset_user_data


class BotBase:
    def __init__(self, keyword, bot):
        self.keyword = keyword
        self.access_level = AccessLevel.USER
        self.bot = bot

    def register_plugin_callback(self):
        return self.keyword, self.callback_handler

    def register_plugin_userdata(self):
        return self.keyword, self.userdata_handler

    def register_plugin_reply(self):
        return self.keyword, self.reply_handler

    def callback_handler(self, update, context):
        query = update.callback_query
        data = query.data
        query.answer()
        user = update.effective_user
        user_id, user_name, user_login = user.id, user.first_name, user.username
        level = get_access_level(user_id=user_id)
        if level >= self.access_level:
            params = data.split('_')
            if len(params) > 1:
                f_name = f"handle_callback_{params[0]}_{params[1]}"
            else:
                f_name = f"handle_callback_{params[0]}"
            my_handler = getattr(self, f_name, None)
            if my_handler is not None:
                logging.info(f"{f_name} from: {user_name}({user_id}), level: {level.name}, data: {data}")
                my_handler(update, context, level, user, query)
            else:
                logging.error(f'method not found!: {f_name}')

    def userdata_handler(self, update, context):
        if context.user_data.get(self.keyword, None) is True:
            user = update.effective_user
            user_id, user_name, user_login = user.id, user.first_name, user.username
            level = get_access_level(user_id=user_id)
            if level >= self.access_level:
                params = context.user_data.get('action', 'unknown')
                f_name = f"handle_userdata_{params}"
                my_handler = getattr(self, f_name, None)
                if my_handler is not None:
                    logging.info(f"{f_name} from: {user_name}({user_id}), level: {level.name}")
                    my_handler(update, context, level, user)
                else:
                    logging.error(f'method not found!: {f_name}')

        else:
            logging.error(f'message redirected to plugin, but context not found')
            reset_user_data(context.user_data)

    def reply_handler(self, update, context):
        searchfor = f'{self.keyword}_reply'
        found = ""
        for key in context.user_data.keys():
            if key.startswith(searchfor):
                found = key
                break
        if found != "":
            user = update.effective_user
            user_id, user_name, user_login = user.id, user.first_name, user.username
            level = get_access_level(user_id=user_id)
            if level >= self.access_level:
                f_name = f"handle_reply_{found}"
                my_handler = getattr(self, f_name, None)
                if my_handler is not None:
                    logging.info(f"{f_name} from: {user_name}({user_id}), level: {level.name}")
                    my_handler(update, context, level, user)
                else:
                    logging.error(f'method reply not found!: {f_name}')

    def sendAnyMessage(self, chat_id, text="", message_type=MessageType.TEXT, file_id=None, parse_mode='MarkdownV2',
                       reply_markup=None):
        if message_type == MessageType.TEXT:
            res = self.bot.sendMessage(chat_id=chat_id, text=text, parse_mode=parse_mode, reply_markup=reply_markup)
        elif message_type == MessageType.AUDIO:
            res = self.bot.sendAudio(chat_id=chat_id, audio=file_id, caption=text, parse_mode=parse_mode,
                                     reply_markup=reply_markup)
        elif message_type == MessageType.PHOTO:
            res = self.bot.sendPhoto(chat_id=chat_id, photo=file_id, caption=text, parse_mode=parse_mode,
                                     reply_markup=reply_markup)
        elif message_type == MessageType.VIDEO:
            res = self.bot.sendVideo(chat_id=chat_id, video=file_id, caption=text, parse_mode=parse_mode,
                                     reply_markup=reply_markup)
        elif message_type == MessageType.ANIMATION:
            res = self.bot.sendAnimation(chat_id=chat_id, animation=file_id, caption=text, parse_mode=parse_mode,
                                         reply_markup=reply_markup)
        elif message_type == MessageType.DOCUMENT:
            res = self.bot.sendDocument(chat_id=chat_id, document=file_id, caption=text, parse_mode=parse_mode,
                                        reply_markup=reply_markup)
        elif message_type == MessageType.STICKER:
            res = self.bot.sendSticker(chat_id=chat_id, sticker=file_id, reply_markup=reply_markup)
        else:
            logging.error(f'Unknown message type: {message_type}')
            res = None
        return res
