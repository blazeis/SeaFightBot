import logging
import random
from random import randint

from telegram import ParseMode

from libs.static import Direction, header, fields, symbols, FieldState
from libs.utils import reset_user_data, get_access_level, get_keyboard, parse_message
from modules.BotBase import BotBase


class BotGame(BotBase):
    def __init__(self, bot):
        super().__init__(keyword='game', bot=bot)

    def handle_callback_game_place(self, update, context, level, user, query):
        reset_user_data(context.user_data)
        text = "Я автоматически разместил твои корабли. Нраицца?\n\n"
        arr = self.__generate_field()
        text += f"{header}"
        for y in range(10):
            for x in range(10):
                if x == 0:
                    text += fields[y]
                text += symbols[arr[x][y]]
            text += "\n"
        context.user_data['my_ships'] = arr
        keyboard = get_keyboard(level, 'placed')
        query.edit_message_text(text=text, reply_markup=keyboard)

    def handle_callback_game_start(self, update, context, level, user, query):
        text = "Корабли бота размещены\. Пиши, в какое место направим торпеды?\n_Два числа через пробел, " \
               "вначале вертикаль, потом горизонталь_\n"
        arr = self.__generate_field()
        context.user_data['bot_ships'] = arr
        context.user_data['bot_hitted'] = self.generate_empty_list()
        context.user_data['my_hitted'] = self.generate_empty_list()
        text += self.__form_field_graph(context.user_data['my_ships'])
        text += self.__form_field_graph(context.user_data['bot_hitted'])
        keyboard = get_keyboard(level, 'restart')
        context.user_data['game'] = True
        context.user_data['action'] = 'hit'
        msg = query.edit_message_text(text=text, reply_markup=keyboard, parse_mode=ParseMode.MARKDOWN_V2)
        try:
            context.user_data['delmessage'] = msg.message_id
        except Exception as e:
            logging.info(e)

    def handle_userdata_hit(self, update, context, level, user):
        message_text, message_type, file_id = parse_message(update.message)
        keyboard = get_keyboard(get_access_level(user.id), 'restart')
        bot_hit = True
        try:
            x, y = message_text.split()
            x = int(x) - 1
            y = int(y) - 1
            if x > 9 or y > 9 or x < 0 or y < 0:
                raise ValueError
        except Exception as e:
            text = 'Не смог разобрать, что ты мне написал. я жду: "3 5" без кавычек, ' \
                   'где 3 -- координата по вертикали, 5 -- по горизонтали.'
            text += self.__form_field_graph(context.user_data['my_ships'])
            text += self.__form_field_graph(context.user_data['bot_hitted'])
            update.message.reply_text(text, reply_markup=keyboard)
            return
        hit = context.user_data['bot_ships'][x][y]
        if hit == FieldState.SHIP:
            context.user_data['bot_hitted'][x][y] = FieldState.DAMAGED
            context.user_data['bot_ships'][x][y] = FieldState.DAMAGED
            text = 'Торпеда попала в корабль!'
            coords = self.__get_ship_coords(context.user_data['bot_ships'], x, y)
            killed = self.__is_ship_killed(context.user_data['bot_ships'], coords)
            if killed is True:
                text += '\nКорабль уничтожен!'
                self.__kill_ship(coords, context.user_data['bot_hitted'])
                context.user_data['killed'] = context.user_data.get('killed', 0) + 1
            text += "\nСтреляй ещё раз"
        else:
            if hit in [FieldState.DAMAGED, FieldState.KILLED, FieldState.MISSED]:
                text = 'И зачем ты туда стрелял?'
            else:
                text = 'Мимо!'
            context.user_data['bot_hitted'][x][y] = FieldState.MISSED
            last_hit = context.user_data.get('last_hit', {})
            res = True
            while res is True and context.user_data.get('killed', 0) < 10:
                res = self.__bot_step(context.user_data['my_hitted'],
                                      context.user_data['my_ships'],
                                      last_hit)
                context.user_data['last_hit'] = last_hit
                if res is True:
                    context.user_data['my_killed'] = context.user_data.get('killed', 0) + 1
        c_arr = context.user_data['my_ships'].copy()
        for i in range(10):
            for j in range(10):
                if context.user_data['my_hitted'][i][j] in [FieldState.KILLED, FieldState.DAMAGED, FieldState.MISSED]:
                    c_arr[i][j] = context.user_data['my_hitted'][i][j]
        text += self.__form_field_graph(c_arr)
        if context.user_data.get('killed', 0) == 10:
            for x in range(10):
                for y in range(10):
                    if context.user_data['bot_hitted'][x][y] not in [FieldState.KILLED, FieldState.MISSED]:
                        context.user_data['bot_hitted'][x][y] = FieldState.MISSED
            text += "\nПоздравляю, ты победил!"
        if context.user_data.get('my_killed', 0) == 10:
            for x in range(10):
                for y in range(10):
                    if context.user_data['bot_hitted'][x][y] not in [FieldState.KILLED, FieldState.MISSED]:
                        context.user_data['bot_hitted'][x][y] = FieldState.MISSED
            text += "\nСочувствую, бот победил!"
        text += self.__form_field_graph(context.user_data['bot_hitted'])
        update.message.reply_text(text, reply_markup=keyboard)

    def __bot_step(self, arr_hitted, arr_ships, last_hit):
        if len(last_hit) == 0:
            available_points = []
            for x in range(10):
                for y in range(10):
                    if arr_hitted[x][y] == FieldState.WATER:
                        available_points.append([x, y])
            hit = random.choice(available_points)
            x, y = hit[0], hit[1]
            if arr_ships[x][y] != FieldState.SHIP:
                arr_hitted[x][y] = FieldState.MISSED
                print(f'missed in {x}-{y}')
                return False
            else:
                arr_hitted[x][y] = FieldState.DAMAGED
                arr_ships[x][y] = FieldState.DAMAGED
                print(f'damaged in {x}-{y}')
                last_hit['x'], last_hit['y'] = x, y
                coords = self.__get_ship_coords(arr_ships, x, y)
                killed = self.__is_ship_killed(arr_ships, coords)
                if killed is True:
                    print(f'killed in {x}-{y}')
                    self.__kill_ship(coords, arr_hitted)
                    last_hit.clear()
                    return True
        x, y = last_hit['x'], last_hit['y']
        print(f'starting from {x}-{y}')
        arr = ['x_down', 'x_up', 'y_down', 'y_up']
        for elem in range(4):
            ch = random.choice(arr)
            arr.remove(ch)
            my_handler = getattr(self, ch, None)
            result = my_handler(arr_hitted, arr_ships, x, y, last_hit)
            if result is not None:
                return result

    def __form_field_graph(self, arr):
        res = f"\n{header}"
        for y in range(10):
            for x in range(10):
                if x == 0:
                    res += fields[y]
                res += symbols[arr[x][y]]
            res += "\n"
        return res

    def __get_ship_coords(self, arr, x, y):
        ship_coords = [(x, y)]
        x_t = x
        while x_t >= 1:
            x_t -= 1
            if arr[x_t][y] in [FieldState.SHIP, FieldState.DAMAGED]:
                ship_coords.append((x_t, y))
            else:
                break
        x_t = x
        while x_t <= 8:
            x_t += 1
            if arr[x_t][y] in [FieldState.SHIP, FieldState.DAMAGED]:
                ship_coords.append((x_t, y))
            else:
                break
        y_t = y
        while y_t >= 1:
            y_t -= 1
            if arr[x][y_t] in [FieldState.SHIP, FieldState.DAMAGED]:
                ship_coords.append((x, y_t))
            else:
                break
        y_t = y
        while y_t <= 8:
            y_t += 1
            if arr[x][y_t] in [FieldState.SHIP, FieldState.DAMAGED]:
                ship_coords.append((x, y_t))
            else:
                break
        print(ship_coords)
        return ship_coords

    def __is_ship_killed(self, arr, ship_coords):
        for coord in ship_coords:
            if arr[coord[0]][coord[1]] == FieldState.SHIP:
                return False
        return True

    def __kill_ship(self, coords, arr):
        xs = set()
        ys = set()
        for elem in coords:
            xs.add(elem[0])
            ys.add(elem[1])
        direction = Direction.HORIZONTAL if len(xs) > len(ys) else Direction.VERTICAL
        length = max(len(xs), len(ys))
        x = min(xs)
        y = min(ys)
        rect_start_x = max(x - 1, 0)
        rect_start_y = max(y - 1, 0)
        rect_end_x = min(x + length if direction == Direction.HORIZONTAL else x + 1, 9)
        rect_end_y = min(y + length if direction == Direction.VERTICAL else y + 1, 9)
        x_ship = [x] if direction == Direction.VERTICAL else [i for i in range(x, x + length)]
        y_ship = [y] if direction == Direction.HORIZONTAL else [j for j in range(y, y + length)]
        for rect_x in range(rect_start_x, rect_end_x + 1):
            for rect_y in range(rect_start_y, rect_end_y + 1):
                arr[rect_x][rect_y] = FieldState.KILLED if (rect_x in x_ship) and (rect_y in y_ship) else FieldState.MISSED

    def __get_ship(self, arr, length):
        direction = Direction(randint(0, 1))
        for tries in range(3000):
            x = randint(0, 10 - length if direction == Direction.HORIZONTAL else 9)
            y = randint(0, 10 - length if direction == Direction.VERTICAL else 9)
            if self.__put(arr, x, y, length, direction):
                return tries
        return 3000

    def __put(self, arr, x, y, length, direction):
        rect_start_x = max(x - 1, 0)
        rect_start_y = max(y - 1, 0)
        rect_end_x = min(x + length if direction == Direction.HORIZONTAL else x + 1, 9)
        rect_end_y = min(y + length if direction == Direction.VERTICAL else y + 1, 9)
        x_ship = [x] if direction == Direction.VERTICAL else [i for i in range(x, x + length)]
        y_ship = [y] if direction == Direction.HORIZONTAL else [j for j in range(y, y + length)]
        for elem in range(length):
            ship_coord_x = x + elem if direction == Direction.HORIZONTAL else x
            ship_coord_y = y + elem if direction == Direction.VERTICAL else y
            if arr[ship_coord_x][ship_coord_y] != FieldState.WATER:
                # print(f"check_coords: {ship_coord_x}, {ship_coord_y}, {arr[ship_coord_x][ship_coord_y]}")
                return False
            print(f"check_coords: {ship_coord_x + 1}, {ship_coord_y + 1}")
        for rect_x in range(rect_start_x, rect_end_x + 1):
            for rect_y in range(rect_start_y, rect_end_y + 1):
                arr[rect_x][rect_y] = FieldState.SHIP if (rect_x in x_ship) and (rect_y in y_ship) else FieldState.NEAR
        return True

    def generate_empty_list(self):
        arr = []
        for y in range(10):
            row = []
            for x in range(10):
                row.append(FieldState.WATER)
            arr.append(row)
        return arr

    def __generate_field(self):
        arr = self.generate_empty_list()

        count = self.__get_ship(arr, 4)
        count += self.__get_ship(arr, 3)
        count += self.__get_ship(arr, 3)
        count += self.__get_ship(arr, 2)
        count += self.__get_ship(arr, 2)
        count += self.__get_ship(arr, 2)
        count += self.__get_ship(arr, 1)
        count += self.__get_ship(arr, 1)
        count += self.__get_ship(arr, 1)
        count += self.__get_ship(arr, 1)
        return arr

    def x_down(self, arr_hitted, arr_ships, x, y, last_hit):
        print(f'called x_down()')
        x_t = x
        while x_t >= 1:
            x_t -= 1
            if arr_hitted[x_t][y] in [FieldState.WATER]:
                if arr_ships[x_t][y] != FieldState.SHIP:
                    arr_hitted[x_t][y] = FieldState.MISSED
                    print(f'missed in {x_t}-{y}(x_t-)')
                    return False
                else:
                    arr_hitted[x_t][y] = FieldState.DAMAGED
                    arr_ships[x_t][y] = FieldState.DAMAGED
                    print(f'damaged in {x_t}-{y}(x_t-)')
                    coords = self.__get_ship_coords(arr_ships, x_t, y)
                    killed = self.__is_ship_killed(arr_ships, coords)
                    if killed is True:
                        self.__kill_ship(coords, arr_hitted)
                        last_hit.clear()
                        return True
            else:
                break

    def x_up(self, arr_hitted, arr_ships, x, y, last_hit):
        print(f'called x_up()')
        x_t = x
        while x_t <= 8:
            x_t += 1
            if arr_hitted[x_t][y] in [FieldState.WATER]:
                if arr_ships[x_t][y] != FieldState.SHIP:
                    arr_hitted[x_t][y] = FieldState.MISSED
                    print(f'missed in {x_t}-{y}(x_t+)')
                    return False
                else:
                    arr_hitted[x_t][y] = FieldState.DAMAGED
                    arr_ships[x_t][y] = FieldState.DAMAGED
                    print(f'damaged in {x_t}-{y}(x_t+)')
                    coords = self.__get_ship_coords(arr_ships, x_t, y)
                    killed = self.__is_ship_killed(arr_ships, coords)
                    if killed is True:
                        self.__kill_ship(coords, arr_hitted)
                        last_hit.clear()
                        return True
            else:
                break

    def y_down(self, arr_hitted, arr_ships, x, y, last_hit):
        print(f'called y_down()')
        y_t = y
        while y_t >= 1:
            y_t -= 1
            if arr_hitted[x][y_t] in [FieldState.WATER]:
                if arr_ships[x][y_t] != FieldState.SHIP:
                    arr_hitted[x][y_t] = FieldState.MISSED
                    print(f'missed in {x}-{y_t}(y_t-)')
                    return False
                else:
                    arr_hitted[x][y_t] = FieldState.DAMAGED
                    arr_ships[x][y_t] = FieldState.DAMAGED
                    print(f'damaged in {x}-{y_t}(y_t-)')
                    coords = self.__get_ship_coords(arr_ships, x, y_t)
                    killed = self.__is_ship_killed(arr_ships, coords)
                    if killed is True:
                        self.__kill_ship(coords, arr_hitted)
                        last_hit.clear()
                        return True
            else:
                break

    def y_up(self, arr_hitted, arr_ships, x, y, last_hit):
        print(f'called y_up()')
        y_t = y
        while y_t <= 8:
            y_t += 1
            if arr_hitted[x][y_t] in [FieldState.WATER]:
                if arr_ships[x][y_t] != FieldState.SHIP:
                    arr_hitted[x][y_t] = FieldState.MISSED
                    print(f'missed in {x}-{y_t}(y_t+)')
                    return False
                else:
                    arr_hitted[x][y_t] = FieldState.DAMAGED
                    arr_ships[x][y_t] = FieldState.DAMAGED
                    print(f'damaged in {x}-{y_t}(y_t+)')
                    coords = self.__get_ship_coords(arr_ships, x, y_t)
                    killed = self.__is_ship_killed(arr_ships, coords)
                    if killed is True:
                        self.__kill_ship(coords, arr_hitted)
                        last_hit.clear()
                        return True
            else:
                break